FROM "registry.gitlab.com/gitlab-org/cluster-integration/helm-install-image:helm-3.13.3-kube-1.29.0-alpine-3.19.0"

ARG TARGETARCH
ARG HELMFILE_VERSION="0.146.0"
ARG KUSTOMIZE_VERSION="4.5.4"
ARG HELM_DIFF_VERSION="3.4.2"
ARG HELM_GIT_VERSION="0.10.0"

RUN wget https://github.com/helmfile/helmfile/releases/download/v${HELMFILE_VERSION}/helmfile_${HELMFILE_VERSION}_linux_${TARGETARCH}.tar.gz \
  && tar xf helmfile_${HELMFILE_VERSION}_linux_${TARGETARCH}.tar.gz \
  && mv helmfile /usr/local/bin/helmfile \
  && chmod u+x /usr/local/bin/helmfile

RUN wget -qO- https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize/v${KUSTOMIZE_VERSION}/kustomize_v${KUSTOMIZE_VERSION}_linux_${TARGETARCH}.tar.gz | tar -xzv -C /usr/local/bin

# [make, go-1.14, golint] are dependencies for building helm-diff from source.
RUN apk add --no-cache bash make go \
  && go install golang.org/x/lint/golint@latest
RUN cp $HOME/go/bin/golint /usr/local/bin/golint

# Builds helm-diff from source and removes dependencies afterwards.
#
# CGO_ENABLED=0 is necessary otherwise the build fails for ARM64
RUN wget https://github.com/databus23/helm-diff/archive/refs/tags/v${HELM_DIFF_VERSION}.zip \
  && unzip v${HELM_DIFF_VERSION}.zip && rm v${HELM_DIFF_VERSION}.zip \
  && cd helm-diff-${HELM_DIFF_VERSION} \
  && GOARCH=${TARGETARCH} CGO_ENABLED=0 make install/helm3 \
  && cd .. \
  && rm -rf helm-diff-${HELM_DIFF_VERSION} \
  && rm /usr/local/bin/golint \
  && apk del make go

RUN helm plugin install https://github.com/aslafy-z/helm-git.git --version ${HELM_GIT_VERSION}

COPY src/ /opt/cluster-applications/

RUN ln -s /opt/cluster-applications/bin/* /usr/local/bin/
