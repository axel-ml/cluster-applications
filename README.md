# Cluster Applications

End goal: Execute all helm operations as GitLab CI yml in the connected
cluster repo (vs running on server).

## Usage

Please see the docs on [how to install applications](https://docs.gitlab.com/ee/user/clusters/management_project_template.html).

## Development

Scripts in this repository follow GitLab's
[shell scripting guide](https://docs.gitlab.com/ee/development/shell_scripting_guide/)
and enforces `shellcheck` and `shfmt`.

## Testing

This project's CI uses [`k3s`](https://k3s.io/) clusters to test installing
applications.

## Included scripts

| Script | Description |
|--|--|
| `gl-ensure-namespace {namespace}` | Creates the given namespace if it does not exist and adds the necessary label for the [Cilium](https://github.com/cilium/cilium/) app network policies. |
| `gl-helmfile {arguments}`| A thin wrapper that triggers the [Helmfile](https://github.com/roboll/helmfile) command. |

## Contributing and Code of Conduct

Please see [CONTRIBUTING.md](CONTRIBUTING.md)

## Git Commit Guidelines

This project uses [Semantic Versioning](https://semver.org). We use commit
messages to automatically determine the version bumps, so they should adhere to
the conventions of [Conventional Commits (v1.0.0-beta.2)](https://www.conventionalcommits.org/en/v1.0.0-beta.2/).

### TL;DR

- Commit messages starting with `fix: ` trigger a patch version bump
- Commit messages starting with `feat: ` trigger a minor version bump
- Commit messages starting with `BREAKING CHANGE: ` trigger a major version bump.

## Automatic versioning

Each push to `master` triggers a [`semantic-release`](https://semantic-release.gitbook.io/semantic-release/)
CI job that determines and pushes a new version tag (if any) based on the
last version tagged and the new commits pushed. Notice that this means that if a
Merge Request contains, for example, several `feat: ` commits, only one minor
version bump will occur on merge. If your Merge Request includes several commits
you may prefer to ignore the prefix on each individual commit and instead add
an empty commit summarizing your changes like so:

```
git commit --allow-empty -m '[BREAKING CHANGE|feat|fix]: <changelog summary message>'
```
